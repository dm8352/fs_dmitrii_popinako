--------------������� �7---------------
/* ���� ������� �������� GRATH_SRC � TEMP_GRATH (��������� ������ ����)
� ��������� ������� ������ ������� ����� ������, ��������� � ���������� ������ �� ���� weight_src
 */
/*
CREATE TABLE GRATH_SRC (
id number (1),
From_src number (3),
To_src number (3),
Weight_src number (3)
);
CREATE TABLE TEMP_GRATH (
From_temp number (3),
To_temp number (3),
weight number (3)
);
INSERT INTO GRATH_SRC (id, From_src, To_src, Weight_src) VALUES (1, 100, 102, 100);
INSERT INTO GRATH_SRC (id, From_src, To_src, Weight_src) VALUES (2, 100, 101, 400);
INSERT INTO GRATH_SRC (id, From_src, To_src, Weight_src) VALUES (3, 101, 102, 200);
INSERT INTO GRATH_SRC (id, From_src, To_src, Weight_src) VALUES (4, 300, 301, 300);
INSERT INTO GRATH_SRC (id, From_src, To_src, Weight_src) VALUES (5, 300, 302, 400);
INSERT INTO GRATH_SRC (id, From_src, To_src, Weight_src) VALUES (6, 301, 302, 500);

INSERT INTO TEMP_GRATH (From_temp, To_temp, weight) VALUES (100, 101, 200);
INSERT INTO TEMP_GRATH (From_temp, To_temp, weight) VALUES (200, 201, 200);
INSERT INTO TEMP_GRATH (From_temp, To_temp, weight) VALUES (300, 301, 300);
INSERT INTO TEMP_GRATH (From_temp, To_temp, weight) VALUES (300, 302, 400);
INSERT INTO TEMP_GRATH (From_temp, To_temp, weight) VALUES (301, 302, 600);
*/

SELECT g.FROM_SRC, g.to_src, g.weight_src FROM GRATH_SRC g
MINUS
SELECT t.from_temp, t.to_temp, t.weight FROM TEMP_GRATH t 
UNION 
(SELECT from_temp, to_temp, weight FROM TEMP_GRATH WHERE from_temp NOT IN(SELECT FROM_SRC FROM GRATH_SRC));
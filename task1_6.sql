--------------������� �6---------------
/* ������� �������, ID ������ � �������� ���� �����������, �������� ������� �����
����������� �������� ������-���� ������ */

SELECT last_name, job_id, salary
FROM employees e
WHERE salary = (SELECT MIN(salary) FROM employees cd
WHERE e.department_id = cd.department_id)
ORDER BY salary DESC, last_name, first_name, employee_id;

SELECT last_name, job_id, salary
FROM employees e
WHERE salary
IN 
(SELECT MIN (salary) FROM employees cd WHERE e.department_id = cd.department_id)
ORDER BY salary DESC, last_name, first_name, employee_id;